FROM node:10-alpine
WORKDIR /app
ARG port
ARG token_key
ARG check_auth
ENV Port=${port} TokenKey=${token_key} CheckAuth=${check_auth}
COPY ./ /app
RUN npm install
RUN npm run build
CMD [ "node","publish/src/gateway/server.js" ]