import { Container } from "inversify";
import { MongoRepository } from "./mongo";
import { IRepository } from "../core/abstractions/repository";

export function RegisterRepositoryTypes (container:Container):void{
    container.bind<IRepository>("IRepository").to(MongoRepository).inSingletonScope();
}