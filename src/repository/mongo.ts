
import { MongoClient, Db, Collection } from "mongodb";
import { injectable } from "inversify";
import { IRepository } from "../core/abstractions/repository";

@injectable()
export class MongoRepository implements IRepository{  
   
    private client: MongoClient;
    private dbName: string;  

    constructor(dbName:string,client:MongoClient){
        this.client=client;
        this.dbName = dbName; 
    }    

    async getDb(): Promise<Db> {
        
        return this.client.db(this.dbName);
    }

    async getCollection<T>(colName: string): Promise<Collection<T>> {
        const db = await this.getDb();
        return db.collection<T>(colName)
    }

    async findOneById<T>(collectionName: string, id: string): Promise<T | null> {
        const col = await this.getCollection(collectionName);
        return col.findOne<T>({ _id: id });
    }

    async find<T>(collectionName: string, query: any,sort:any=null,skip:number=0,take:number=0): Promise<T[]> {
        const col = await this.getCollection(collectionName);
        let cursor = col.find(query);
        if(sort!=null)
          cursor = cursor.sort(sort);
        if(skip>0)
          cursor = cursor.skip(skip);
        if(take>0)
          cursor = cursor.limit(take);    
        return await cursor.toArray() as T[];
    }

    async findOneByQuery<T>(collectionName: string, query: any): Promise<T | null> {
        const col = await this.getCollection(collectionName);
        return col.findOne<T>(query);
    }

    async save<T>(collectionName: string, id: string, item: T): Promise<boolean> {           
        const col = await this.getCollection(collectionName);
        const result = await col.updateOne({ _id: id },{$set:item}, { upsert: true });        
        return result.result.ok == 1;
    }

    async getCount(collectionName: string, query: any): Promise<number> {
        const col = await this.getCollection(collectionName);
        return await col.countDocuments(query,undefined);
    }
}