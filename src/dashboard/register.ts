import { IMessageRouter } from "../core/abstractions/routing";
import { Container } from "inversify";
import widgetCreatedFactory  from "./handlers/event/widgetCreated";

export function RegisterDashboardRouters (container:Container,router:IMessageRouter):void{
    router.registerEventHandler("widgetCreated",widgetCreatedFactory(),null);
}