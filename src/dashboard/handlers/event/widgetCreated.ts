import { WidgetCreated } from "../../../widget/messages/event/widgetCreated";

export default ()=> {
    return (data:WidgetCreated):void=>{
        console.log("data arrived to dashboard handler as "+ JSON.stringify(data));
    }
}