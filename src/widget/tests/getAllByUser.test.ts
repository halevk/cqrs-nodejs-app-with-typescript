import {GetAllByUserValidator, GetAllByUser} from "../messages/query/getAllByUser";
import { expect } from "chai";
import mocha from "mocha";
import handlerFactory from "../handlers/query/getAllByUser";

describe("getallbyuser",()=>{
    it("should_return_error_if_userid_in_model_empty",()=> {
        var request = new GetAllByUser();        
        const validationResult = GetAllByUserValidator(request);
        expect(false).equal(validationResult.success);        
    });

    it("should_return_all_widget_for_user_test",async ()=> {
         const handler = handlerFactory();
         var request = new GetAllByUser();
         request.userId="1";
         const response = await handler(request);
         expect(1).equal(response.length);
    });
});