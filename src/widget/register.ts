import { IMessageRouter, IMessageBroker } from "../core/abstractions/routing";
import GetAllByUserHandler from "./handlers/query/getAllByUser";
import { GetAllByUserValidator } from "./messages/query/getAllByUser";
import CreateNewHandler from "./handlers/command/createNew";
import { Container } from "inversify";

export function RegisterWidgetRouters (container:Container,router:IMessageRouter):void{
    router.registerCommandHandler("/widget/getallbyuser",GetAllByUserHandler(),GetAllByUserValidator); 
    router.registerCommandHandler("/widget/new",CreateNewHandler(container.get<IMessageBroker>("IMessageBroker")),null)   
}