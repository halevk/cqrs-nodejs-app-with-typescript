export interface WidgetCreated{
    name:string,
    type:string,
    createdby:string
}