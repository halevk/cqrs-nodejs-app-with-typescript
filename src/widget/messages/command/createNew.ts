import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class CreateNew{
    name:string="";
    active:boolean=false;
    type:string=""    
}

export interface CreateNewResponse{
    success:boolean,
    errors:string[] | null
}


export function CreateNewValidator(request:CreateNew):ValidationResult{
    const err:string[]=[];
    if(IsNullOrEmptyOrWhitespace(request.name)) err.push("name is required");    
    if(IsNullOrEmptyOrWhitespace(request.type)) err.push("widget type is required");
    const success = err.length==0;
    return {errors:err,success:success};

}
