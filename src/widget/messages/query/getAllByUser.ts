import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class GetAllByUser{
    userid:string=""
 }
 
 export interface GetAllByUserResponse{
     id:string,
     name:string
 }

 export function GetAllByUserValidator(request:GetAllByUser):ValidationResult{    
    const errors:string[]=[];
     if(IsNullOrEmptyOrWhitespace(request.userid))
        errors.push("userid is required");
    const success = errors.length==0;
    return {success:success,errors:errors};
 }