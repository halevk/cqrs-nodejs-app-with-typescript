import { CreateNew, CreateNewResponse } from "../../messages/command/createNew";
import { IAppContext } from "../../../core/abstractions/appcontext";
import { IMessageBroker } from "../../../core/abstractions/routing";
import { WidgetCreated } from "../../messages/event/widgetCreated";
const httpContext = require("express-http-context");

export default (broker:IMessageBroker)=> {
     return async(request:CreateNew):Promise<CreateNewResponse>=>{
        const ctx:IAppContext = httpContext.get("context");
        
        broker.publish<WidgetCreated>("widgetCreated",{name:request.name,type:request.type,createdby:ctx.userName});
        return Promise.resolve<CreateNewResponse>({errors:null,success:true});
     }
}