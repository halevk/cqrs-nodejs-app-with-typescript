import { Container } from "inversify";
import { Request, Response, NextFunction } from "express";
import { IMessageBroker } from "../../core/abstractions/routing";


export default(container:Container)=> {
    const broker = container.get<IMessageBroker>("IMessageBroker");
    return async(req:Request,resp:Response,next:NextFunction) => {
        const route = req.url.replace("/","");
        const result = await broker.send(route,req.body);
        resp.send(result);
    }
}