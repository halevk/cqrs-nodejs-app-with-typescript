import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
const uuidv4 = require("uuid/v4");

export default (tokenKey: string, httpContext: any, checkAuth: boolean) => {
    return (req: Request, resp: Response, next: NextFunction) => {
        if (checkAuth && req.url.indexOf("/auth") == -1) {
            const authHeader = req.headers.authorization || "";
            if (authHeader == null || authHeader == undefined || authHeader == "") {
                resp.status(401).send({ status: false, error: "auth token is required" });
                return;
            }
            const [type, token] = authHeader.split(" ");
            if (type.toLowerCase() != "bearer") {
                resp.status(401).send({ status: false, error: "auth type must be bearer" });
                return;
            }
            try {
                const decoded = jwt.verify(token, tokenKey) as any;
                httpContext.set("context", {
                    userId: decoded.id,
                    userName: decoded.name,
                    requestId: uuidv4()
                });
            } catch (err) {
                resp.status(401).send({ status: false, error: "token is invalid" });
                return;
            }
            next();
            return;
        } else
            httpContext.set("context", {
                userId: "1",
                userName: "test",
                requestId: uuidv4()
            });
        next();
    }
}