import { Request, Response, NextFunction } from "express";

export default () => {
    return (req: Request, resp: Response, next: NextFunction) => {
        if (req.method.toLowerCase() != "post") {
            resp.send("only post methods supported").status(400);            
            return;
        }
        next();        
    }
}