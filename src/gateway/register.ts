import { Container } from "inversify"
import { IMessageRouter } from "../core/abstractions/routing";
import {RegisterMessagingTypes} from "../messaging/register";
import {RegisterRepositoryTypes} from "../repository/register";
import { RegisterWidgetRouters } from "../widget/register";
import { RegisterDashboardRouters } from "../dashboard/register";

export function RegisterTypes():Container {
    const container = new Container();    
    RegisterMessagingTypes(container);
    RegisterRepositoryTypes(container);
    return container;
} 

export function RegisterRoutes(container:Container) {
    const routers = container.get<IMessageRouter>("IMessageRouter");
    RegisterWidgetRouters(container,routers);
    RegisterDashboardRouters(container,routers);
}
