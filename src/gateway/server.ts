require("dotenv").config();
import 'reflect-metadata';
import cors from "cors";
import express from "express";
import bodyParser from "body-parser";
const httpContext = require("express-http-context");
import { RegisterRoutes,RegisterTypes } from "./register";

import authMiddleware from "./middlewares/auth";
import methodMiddleware from "./middlewares/httpmethod";
import genericHandler from "./middlewares/genericHandler";

const port = process.env.Port || 10000;
const tokenKey = process.env.TokenKey || "";
const checkAuth:any = JSON.parse(process.env.CheckAuth || "true");

const container = RegisterTypes();
RegisterRoutes(container);
const app = express();
app.use(cors());
app.use(bodyParser());
app.use(methodMiddleware());
app.use(httpContext.middleware);
app.use(authMiddleware(tokenKey,httpContext,checkAuth));
app.use(genericHandler(container));

app.listen(port,()=> {
    console.log(`server is running on port number ${port}`);
});

process.on('unhandledRejection', (error) => {
    console.log('unhandledRejection', error);
});

