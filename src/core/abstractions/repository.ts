export interface IRepository {
    findOneById<T>(collectionName:string,id: string): Promise<T|null>;
    findOneByQuery<T>(collectionName:string,query:any): Promise<T|null>;
    find<T>(collectionName:string,query:any,sort:any,skip:number,take:number): Promise<T[]>;
    save<T>(collectionName:string,id: string, item: T): Promise<boolean>;
    getCount(collectionName:string,query:any):Promise<number>;
}