export interface IMessageBroker {
    send(route: string, data: any): Promise<any>;
    publish<T>(eventName: string, data:T | any): void;
}

export interface ValidationResult {
    success: boolean,
    errors: string[]
}
export type EventHandler = (data: any) => void;
export type CommandHandler = (data: any) => Promise<any>;
export type ValidatorFunc = (data: any) => ValidationResult;

export interface IHandlerInfo {
    validator: ValidatorFunc | null,
    handler: CommandHandler | EventHandler[]
}

export interface IMessageRouter {
    registerCommandHandler(route: string, handler: CommandHandler, validator: ValidatorFunc | null): void;
    registerEventHandler(eventName: string, handler: EventHandler, validator: ValidatorFunc | null): void;
    getCommandHandler(route: string): IHandlerInfo | undefined;
    getEventHandlers(eventName: string): IHandlerInfo | undefined;
}



