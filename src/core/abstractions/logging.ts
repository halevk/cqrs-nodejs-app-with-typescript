export interface ILogging{
    write<T>(item:T):Promise<boolean>;
}