export interface IEmailService{
    send(receiver:string[],subject:string,message:string,isHtml:boolean):Promise<boolean>
}

