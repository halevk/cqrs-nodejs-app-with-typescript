export interface ICache {
    setSingle(key:string, data:any):Promise<boolean>;
    getSingle<T>(key:string):Promise<T>;
}