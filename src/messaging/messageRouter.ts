import { 
    IMessageRouter, 
    CommandHandler, 
    EventHandler, 
    IHandlerInfo, 
    ValidatorFunc } from "../core/abstractions/routing";
import { injectable } from "inversify";

@injectable()
export class DefaultRouter implements IMessageRouter{    
    private commandHandlers:Map<string,IHandlerInfo>;    
    private eventHandlers:Map<string,IHandlerInfo>;
    constructor(){
        this.commandHandlers=new Map<string,any>();
        this.eventHandlers=new Map<string,any>();
    }

    registerCommandHandler(route: string, handler: CommandHandler, validator: ValidatorFunc | null): void {
        if(this.commandHandlers.has(route)) return;
        this.commandHandlers.set(route,{validator:validator,handler:handler});
    }    

    registerEventHandler(eventName: string, handler: EventHandler, validator: ValidatorFunc | null): void {
        if(this.eventHandlers.has(eventName)){
            const handlerInfo = this.eventHandlers.get(eventName) as IHandlerInfo;
            (handlerInfo.handler as EventHandler[]).push(handler);
        }else{ 
            const handlers:EventHandler[] = [handler];
            this.eventHandlers.set(eventName,{validator:validator,handler:handlers});
        }        
    }

    getCommandHandler(route: string): IHandlerInfo | undefined | undefined {
        if(this.commandHandlers.has(route)) return this.commandHandlers.get(route);
        return undefined;
    }
    
    getEventHandlers(eventName: string): IHandlerInfo | undefined {
        if(this.eventHandlers.has(eventName)) return this.eventHandlers.get(eventName);
        return undefined;
    }   
    
}