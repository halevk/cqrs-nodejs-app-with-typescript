import { Container } from "inversify";
import { IMessageBroker, IMessageRouter } from "../core/abstractions/routing";
import { InMemoryBroker } from "./in-memory-broker";
import { DefaultRouter } from "./messageRouter";

export function RegisterMessagingTypes (container:Container):void{
    container.bind<IMessageBroker>("IMessageBroker").to(InMemoryBroker).inSingletonScope();
    container.bind<IMessageRouter>("IMessageRouter").to(DefaultRouter).inSingletonScope();
}