
import { injectable, inject } from "inversify";
import {
    IMessageRouter,
    CommandHandler,
    EventHandler,
    IMessageBroker,
} from "../core/abstractions/routing";
import { IAppContext } from "../core/abstractions/appcontext";
const httpContext = require("express-http-context");

@injectable()
export class InMemoryBroker implements IMessageBroker {
    private router: IMessageRouter;
    constructor(@inject("IMessageRouter") router: IMessageRouter) {
        this.router = router;
    }
    async send(route: string, data: any): Promise<any> {
        const handlerInfo = this.router.getCommandHandler(route);
        if (!handlerInfo || !handlerInfo.handler)
            return Promise.resolve({ err: "handler not found" });

        if (handlerInfo.validator) {
            const validationResult = handlerInfo.validator(data);
            if (!validationResult.success)
                return Promise.resolve({ err: validationResult.errors.join(",") });
        }
        const handler = handlerInfo.handler as CommandHandler;
        const ctx:IAppContext = httpContext.get("context") as IAppContext;
        return await this.metric(ctx,data,handler);        
    }

    async log(ctx:IAppContext, data: any, handler: CommandHandler): Promise<any> {
        console.log("request Log");
        const result = await handler(data);
        console.log("response log");
        return result;
    }

    async metric(ctx:IAppContext, data: any, handler: CommandHandler): Promise<any> {
        const start = Date.now();
        const result = await this.log(ctx,data, handler);
        const total = Date.now() - start;
        console.log("total execution :"+ total+" ms");
        return result;
    }

    async publish<T>(eventName: string, data:T|any): Promise<void> {
        const handlerInfo = this.router.getEventHandlers(eventName);
        if (!handlerInfo || !handlerInfo.handler) return;
        const handlers = handlerInfo.handler as EventHandler[];
        for (let handler of handlers) {
            handler(data);
        }
    }
}

